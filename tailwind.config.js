/** @type {import('tailwindcss').Config} */
export default {
  content: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],

  theme: {
    extend: {
      colors: {
        bg: '#fdfdfd',
        grey: '#f2f2f2',
        accent: '#fdbc11',
        brand: '#ec4466',
        tertiary: '#3F53BE'
      }
    }
  },
  plugins: []
}
