# Finnplay Technical Exercise

This is my solution to the technical exercise proposed by Finnplay (Adrian Wragg - 17.04.2024)

## How to run

- First, the backend needs to be running before anything else. This can be done by running the command:

```
npm run backend
```

or

```
yarn backend
```

This will run the extremely simple ExpressJS backend that I implemented for the exercise.

- Once the backend is running, it's time to run the application itself. This is done with:

```
npm run dev
```

or

```
yarn dev
```

The application will run in `http://localhost:5173`.
