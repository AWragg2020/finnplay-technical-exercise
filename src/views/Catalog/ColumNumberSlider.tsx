import { useMemo } from 'react'

interface ColumnNumberSliderProps {
  value: number
  onChange: (columns: number) => void
}

const ColumnNumberSlider = ({
  value = 4,
  onChange
}: ColumnNumberSliderProps) => {
  const options: Array<number> = useMemo(() => [2, 3, 4], [])

  return (
    <div className='flex flex-col w-full p-[10px] gap-[10px]'>
      <span className='text-gray-500'>Columns</span>
      <div className='relative flex flex-row justify-between items-center bg-gray-200 rounded-full h-4 z-20'>
        {options.map(option => (
          <span
            key={option}
            className={`w-6 h-6 rounded-full text-center select-none cursor-pointer z-10 ${
              value >= option ? 'bg-accent' : 'bg-gray-200'
            }`}
            onClick={() => onChange(option)}>
            {option}
          </span>
        ))}
        <div
          className={`absolute top-0 left-0 h-full bg-accent rounded-full z-0 ${
            value === 2
              ? ''
              : value === 3
              ? 'w-[50%]'
              : value === 4
              ? 'w-full'
              : ''
          }`}></div>
      </div>
    </div>
  )
}

export default ColumnNumberSlider
