import _ from 'lodash'
import { useEffect, useState } from 'react'
import useScreenSize from '../../hooks/useScreenSize'
import {
  Game,
  GameGroup,
  setGameGroups,
  setGames,
  setProviders
} from '../../redux/features/dataSlice'
import { useAppDispatch } from '../../redux/hooks'
import { responsive } from '../../utils/breakpoints'

import Header from '../../components/Header'
import Filters from './Filters'
import Grid from './Grid'

const Catalog = () => {
  const { width: screenWidth } = useScreenSize()
  const dispatch = useAppDispatch()

  const [cols, setCols] = useState<number>(screenWidth <= responsive ? 2 : 4)

  const handleChangeColumns = (cols: number) => {
    setCols(cols)
  }

  useEffect(() => {
    const getData = async () => {
      const response = await fetch('http://localhost:5454/data', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      })
      const responseJSON = await response.json()
      const providers = responseJSON.providers
      const gameGroups = responseJSON.groups

      const gameGroupsArrays: Array<number> = gameGroups
        .map((group: GameGroup) => group.games)
        .flat()
      // Games that don't belong to any group are not shown:
      const games = responseJSON.games.filter((game: Game) =>
        gameGroupsArrays.includes(game.id)
      )

      dispatch(setProviders(providers))
      dispatch(setGameGroups(gameGroups))
      dispatch(
        setGames(_.orderBy(games, [game => game.name.toLowerCase()], ['asc']))
      )
    }

    getData()
  }, [])

  return (
    <div
      id='catalog'
      className='flex flex-col h-full w-full'>
      <Header />
      <div
        id='list-and-filters'
        className={`relative flex ${
          screenWidth <= responsive
            ? 'flex-col-reverse px-6 mt-[27px] pt-[215px]'
            : 'flex-row px-20 mt-[75px]'
        } gap-5 h-full w-full`}>
        <Grid columns={cols} />
        <Filters onChangeColumns={handleChangeColumns} />
      </div>
    </div>
  )
}

export default Catalog
