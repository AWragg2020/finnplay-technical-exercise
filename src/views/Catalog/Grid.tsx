import _ from 'lodash'
import { useEffect, useState } from 'react'
import useScreenSize from '../../hooks/useScreenSize'
import { Game } from '../../redux/features/dataSlice'
import { setFilteredGamesLength } from '../../redux/features/filtersSlice'
import { useAppDispatch, useAppSelector } from '../../redux/hooks'
import { responsive } from '../../utils/breakpoints'

interface GridProps {
  columns: number
}

const Grid = ({ columns }: GridProps) => {
  const { width: screenWidth } = useScreenSize()
  const dispatch = useAppDispatch()

  const storeGames = useAppSelector(state => state.data.games)
  const [filteredGames, setFilteredGames] = useState<Array<Game>>([])

  const gameNameSearch = useAppSelector(state => state.filters.gameNameSearch)
  const selectedProviders = useAppSelector(state => state.filters.providers)
  const gameGroups = useAppSelector(state => state.data.gameGroups)
  const selectedGameGroups = useAppSelector(state => state.filters.gameGroups)
  const selectedSortingOption = useAppSelector(
    state => state.filters.sortingOption
  )

  useEffect(() => {
    // All filters empty
    if (
      gameNameSearch.trim() === '' &&
      selectedProviders.length === 0 &&
      selectedGameGroups.length === 0
    ) {
      setFilteredGames(storeGames)
      dispatch(setFilteredGamesLength(storeGames.length))
      return
    }

    if (
      gameNameSearch.trim() !== '' &&
      selectedProviders.length === 0 &&
      selectedGameGroups.length === 0
    ) {
      // Filtered games by name
      const filteredGamesByName = storeGames.filter(game =>
        game.name.toLowerCase().includes(gameNameSearch.toLowerCase().trim())
      )
      dispatch(setFilteredGamesLength(filteredGamesByName.length))
      setFilteredGames(filteredGamesByName)
      return
    }

    // Filtered games by group
    const selectedGameGroupsIds = selectedGameGroups.map(group => group.id)
    const selectedGroupGames = gameGroups
      .filter(group => selectedGameGroupsIds.includes(group.id))
      .map(group => group.games)
      .flat()
    const filteredGamesByGroup = storeGames
      .filter(game => selectedGroupGames.includes(game.id))
      .filter(g =>
        g.name.toLowerCase().includes(gameNameSearch.trim().toLowerCase())
      )

    // Filtered games by provider
    const selectedProviderIds = selectedProviders.map(provider => provider.id)
    const filteredGamesByProvider = storeGames
      .filter(game => selectedProviderIds.includes(game.provider))
      .filter(g =>
        g.name.toLowerCase().includes(gameNameSearch.trim().toLowerCase())
      )

    // Array of found game ids without repetitions
    let foundGamesIds: Array<number> = [
      ...filteredGamesByGroup,
      ...filteredGamesByProvider
    ].map(g => g.id)
    foundGamesIds = [...new Set(foundGamesIds)]

    const foundGamesWithoutRepetitions = storeGames.filter(game =>
      foundGamesIds.includes(game.id)
    )

    dispatch(setFilteredGamesLength(foundGamesWithoutRepetitions.length))
    setFilteredGames(foundGamesWithoutRepetitions)
  }, [selectedProviders, selectedGameGroups, gameNameSearch])

  useEffect(() => {
    if (selectedSortingOption.id === 0) {
      setFilteredGames(
        _.orderBy(filteredGames, [game => game.name.toLowerCase()], ['asc'])
      )
    } else if (selectedSortingOption.id === 1) {
      setFilteredGames(
        _.orderBy(filteredGames, [game => game.name.toLowerCase()], ['desc'])
      )
    } else if (selectedSortingOption.id === 2) {
      setFilteredGames(
        _.orderBy(filteredGames, [game => new Date(game.date)], ['desc'])
      )
    }
  }, [selectedSortingOption])

  useEffect(() => {
    setFilteredGames(storeGames)
    dispatch(setFilteredGamesLength(storeGames.length))
  }, [storeGames])

  return (
    <div
      id='grid-scroll-wrapper'
      className='h-full overflow-y-auto w-full pb-44'>
      <div
        id='grid'
        className={`grid ${
          columns === 2
            ? 'grid-cols-2'
            : columns === 3
            ? 'grid-cols-3'
            : columns === 4
            ? 'grid-cols-4'
            : 'grid-cols-4'
        } gap-5`}>
        {filteredGames.map(game => (
          <img
            alt={game.name}
            src={screenWidth <= responsive ? game.cover : game.coverLarge}
            key={game.id}
            className='rounded-lg'
          />
        ))}
      </div>
    </div>
  )
}

export default Grid
