import _ from 'lodash'
import { useMemo, useRef, useState } from 'react'
import useScreenSize from '../../hooks/useScreenSize'
import {
  resetFilters,
  setFilterGameGroups,
  setFilterProviders,
  setFilterSortingOption,
  setNameSearch
} from '../../redux/features/filtersSlice'
import { useAppDispatch, useAppSelector } from '../../redux/hooks'
import { responsive } from '../../utils/breakpoints'

import Button from '../../components/Button/Button'
import TextInput from '../../components/TextInput'
import ColumnNumberSlider from './ColumNumberSlider'
import FilterCategory, { FilterCategoryOption } from './FilterCategory'

import burger from '../../assets/icons/burger.png'
import search from '../../assets/icons/search.png'

interface FiltersProps {
  onChangeColumns: (cols: number) => void
}

const Filters = ({ onChangeColumns }: FiltersProps) => {
  const { width: screenWidth } = useScreenSize()
  const dispatch = useAppDispatch()

  const searchByNameInputRef = useRef<any>(null)
  const providers = useAppSelector(state => state.data.providers)
  const gameGroups = useAppSelector(state => state.data.gameGroups)
  const filteredGamesLength = useAppSelector(
    state => state.filters.filteredGamesLength
  )

  // Toggle show filters
  const [areFiltersVisible, setAreFiltersVisible] = useState<boolean>(
    screenWidth > responsive
  )

  // Options
  const providerOptions: Array<FilterCategoryOption> = providers.map(
    provider => {
      return {
        name: provider.name,
        id: provider.id
      }
    }
  )
  const gameGroupOptions: Array<FilterCategoryOption> = gameGroups.map(
    group => {
      return {
        name: group.name,
        id: group.id
      }
    }
  )
  const sortingOptions = useMemo<Array<FilterCategoryOption>>(
    () => [
      { name: 'A-Z', id: 0 },
      { name: 'Z-A', id: 1 },
      { name: 'Newest', id: 2 }
    ],
    []
  )

  // Selected options
  const selectedProviders = useAppSelector(state => state.filters.providers)
  const selectedGameGroups = useAppSelector(state => state.filters.gameGroups)
  const selectedSortingOption = useAppSelector(
    state => state.filters.sortingOption
  )
  const [columnsValue, setColumnsValue] = useState<number>(
    screenWidth <= responsive ? 2 : 4
  )

  // Handlers
  // Debounced logic as to not saturate server with API calls
  const handleSearchUpdate = _.debounce((e: any) => {
    dispatch(setNameSearch(e.target.value))
  }, 500)

  const handleProvidersUpdate = (option: FilterCategoryOption) => {
    const selectedProvidersIncludesOption = selectedProviders
      .map(providerOption => providerOption.id)
      .includes(option.id)

    if (selectedProvidersIncludesOption) {
      dispatch(
        setFilterProviders(
          selectedProviders.filter(
            providerOption => providerOption.id !== option.id
          )
        )
      )
    } else {
      dispatch(setFilterProviders([...selectedProviders, option]))
    }
  }

  const handleGameGroupsUpdate = (option: FilterCategoryOption) => {
    const selectedGameGroupsIncludesOption = selectedGameGroups
      .map(groupOption => groupOption.id)
      .includes(option.id)

    if (selectedGameGroupsIncludesOption) {
      dispatch(
        setFilterGameGroups(
          selectedGameGroups.filter(groupOption => groupOption.id !== option.id)
        )
      )
    } else {
      dispatch(setFilterGameGroups([...selectedGameGroups, option]))
    }
  }

  const handleSortingChange = (option: FilterCategoryOption) => {
    dispatch(setFilterSortingOption(option))
  }

  const handleColumnsChange = (option: number) => {
    setColumnsValue(option)
    onChangeColumns(option)
  }

  const handleResetFilters = () => {
    searchByNameInputRef.current.value = ''
    dispatch(resetFilters())
  }

  const toggleShowFilters = () => {
    setAreFiltersVisible(prev => !prev)
  }

  return (
    <div
      id='filters'
      className={`${
        screenWidth <= responsive
          ? 'absolute top-0 w-[89%]'
          : 'min-w-[412px] max-w-[412px]'
      } h-fit flex flex-col gap-8 bg-white border border-grey rounded-md p-8`}>
      <TextInput
        innerRef={searchByNameInputRef}
        onChange={handleSearchUpdate}
        iconSrc={search}
        placeholder='Search'
      />
      {areFiltersVisible && (
        <>
          <FilterCategory
            label='Providers'
            options={providerOptions}
            selectedOptions={selectedProviders}
            onChange={handleProvidersUpdate}
          />
          <FilterCategory
            label='Game groups'
            options={gameGroupOptions}
            selectedOptions={selectedGameGroups}
            onChange={handleGameGroupsUpdate}
          />
          <FilterCategory
            label='Sorting'
            options={sortingOptions}
            selectedOptions={[selectedSortingOption]}
            onChange={handleSortingChange}
          />
          {screenWidth > responsive && (
            <ColumnNumberSlider
              value={columnsValue}
              onChange={handleColumnsChange}
            />
          )}
          <div className='flex flex-row w-full justify-between items-center'>
            <span className='text-gray-500'>
              Games amount: {filteredGamesLength}
            </span>
            <Button
              label='Reset'
              onClick={handleResetFilters}
            />
          </div>
        </>
      )}
      {screenWidth <= responsive && (
        <div
          className='flex flex-row items-center justify-center w-full gap-2 cursor-pointer select-none'
          onClick={toggleShowFilters}>
          <img
            src={burger}
            width='14px'
            height='14px'
          />
          <span className='text-tertiary'>
            {areFiltersVisible ? 'Hide' : 'Show'} filters
          </span>
        </div>
      )}
    </div>
  )
}

export default Filters
