export interface FilterCategoryOption {
  name: string
  id: number
}

interface FilterCategoryProps {
  label: string
  options: Array<FilterCategoryOption>
  selectedOptions: Array<FilterCategoryOption>
  onChange: (option: FilterCategoryOption) => void
}

const FilterCategory = ({
  label,
  options,
  selectedOptions,
  onChange
}: FilterCategoryProps) => {
  const handleToggleOption = (option: FilterCategoryOption) => {
    onChange(option)
  }

  return (
    <div className='flex flex-col p-[10px] gap-[10px]'>
      <span className='text-gray-500'>{label}</span>
      <div className='gap-x-8 gap-y-4 flex flex-row flex-wrap'>
        {options.map(option => (
          <span
            key={option.id}
            className={`select-none cursor-pointer px-2 rounded-md ${
              selectedOptions
                .map(selectedOption => selectedOption.id)
                .includes(option.id)
                ? 'bg-accent text-white'
                : ''
            }`}
            onClick={() => handleToggleOption(option)}>
            {option.name}
          </span>
        ))}
      </div>
    </div>
  )
}

export default FilterCategory
