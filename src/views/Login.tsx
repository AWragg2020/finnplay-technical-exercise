import { useRef, useState } from 'react'
import useScreenSize from '../hooks/useScreenSize'
import { useAppDispatch } from '../redux/hooks'
import { responsive } from '../utils/breakpoints'

import TextInput from '../components/TextInput'

import eye from '../assets/icons/eye.png'
import spinner from '../assets/icons/spinner.png'
import logo from '../assets/images/logo.png'
import { setSessionUser } from '../redux/features/sessionUserSlice'

const Login = () => {
  const { width: screenWidth } = useScreenSize()
  const dispatch = useAppDispatch()

  const userInputRef = useRef<any>()
  const userPasswordRef = useRef<any>()

  const [loggingIn, setLoggingIn] = useState<boolean>(false)

  const handleLogin = async (e: any) => {
    e.preventDefault()

    setLoggingIn(true)
    const response = await fetch('http://localhost:5454/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        user: userInputRef.current.value,
        password: userPasswordRef.current.value
      })
    })

    if (response.status === 401) {
      alert('Incorrect login credentials')
    } else if (response.status === 200) {
      const responseJSON = await response.json()
      dispatch(setSessionUser(responseJSON.sessionUser))
    }
    setLoggingIn(false)
  }

  return (
    <form
      id='login-form'
      className={`flex flex-col gap-10 ${
        screenWidth <= responsive ? 'p-6 w-full' : 'min-w-[412px]'
      }`}
      onSubmit={handleLogin}>
      <div
        id='logo-wrapper'
        className='flex justify-center'>
        <img
          src={logo}
          height='70px'
          width='70px'
        />
      </div>
      <div
        id='login-fields'
        className='flex flex-col gap-5'>
        <TextInput
          innerRef={userInputRef}
          label='Login'
        />
        <TextInput
          innerRef={userPasswordRef}
          placeholder='Password'
          isPassword
          iconSrc={eye}
        />
      </div>
      <button
        id='login-button'
        className='bg-accent h-16 rounded-md flex flex-row justify-center items-center'>
        {loggingIn ? (
          <img
            src={spinner}
            width='16px'
            height='16px'
            className='animate-spin'
          />
        ) : (
          <span>Login</span>
        )}
      </button>
    </form>
  )
}

export default Login
