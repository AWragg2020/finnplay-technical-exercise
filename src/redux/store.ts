import { configureStore } from '@reduxjs/toolkit'
import dataReducer from './features/dataSlice'
import filtersSlice from './features/filtersSlice'
import sessionUserReducer from './features/sessionUserSlice'

export const store = configureStore({
  reducer: {
    sessionUser: sessionUserReducer,
    data: dataReducer,
    filters: filtersSlice
  }
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
