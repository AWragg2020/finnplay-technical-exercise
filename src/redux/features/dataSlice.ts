import { PayloadAction, createSlice } from '@reduxjs/toolkit'

export interface Game {
  id: number
  name: string
  provider: number
  cover: string
  coverLarge: string
  date: string
}

export interface Provider {
  id: number
  name: string
  logo: string
}

export interface GameGroup {
  id: number
  name: string
  games: Array<number>
}

export interface DataSlice {
  games: Array<Game>
  providers: Array<Provider>
  gameGroups: Array<GameGroup>
}

const initialState: DataSlice = {
  games: [],
  providers: [],
  gameGroups: []
}

export const dataSlice = createSlice({
  name: 'data',
  initialState,
  reducers: {
    setGames: (state, action: PayloadAction<Array<Game>>) => {
      state.games = action.payload
    },
    setProviders: (state, action: PayloadAction<Array<Provider>>) => {
      state.providers = action.payload
    },
    setGameGroups: (state, action: PayloadAction<Array<GameGroup>>) => {
      state.gameGroups = action.payload
    }
  }
})

export const { setGames, setProviders, setGameGroups } = dataSlice.actions
export default dataSlice.reducer
