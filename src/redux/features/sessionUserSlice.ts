import type { PayloadAction } from '@reduxjs/toolkit'
import { createSlice } from '@reduxjs/toolkit'

export interface SessionUserState {
  sessionUser: string | null
}

const initialState: SessionUserState = {
  sessionUser: null
}

export const sessionUserSlice = createSlice({
  name: 'counter',
  initialState,
  reducers: {
    resetSessionUser: state => {
      state.sessionUser = null
    },
    setSessionUser: (state, action: PayloadAction<string>) => {
      state.sessionUser = action.payload
    }
  }
})

export const { resetSessionUser, setSessionUser } = sessionUserSlice.actions
export default sessionUserSlice.reducer
