import { createSlice } from '@reduxjs/toolkit'
import { FilterCategoryOption } from '../../views/Catalog/FilterCategory'

export interface FiltersState {
  gameNameSearch: string
  providers: Array<FilterCategoryOption>
  gameGroups: Array<FilterCategoryOption>
  sortingOption: FilterCategoryOption
  filteredGamesLength: number
}

const initialState: FiltersState = {
  gameNameSearch: '',
  providers: [],
  gameGroups: [],
  sortingOption: { id: 0, name: 'A-Z' },
  filteredGamesLength: 0
}

export const filtersSlice = createSlice({
  name: 'filters',
  initialState,
  reducers: {
    resetFilters: state => {
      state.gameNameSearch = ''
      state.providers = []
      state.gameGroups = []
      state.sortingOption = initialState.sortingOption
    },
    setNameSearch: (state, action) => {
      state.gameNameSearch = action.payload
    },
    setFilterProviders: (state, action) => {
      state.providers = action.payload
    },
    setFilterGameGroups: (state, action) => {
      state.gameGroups = action.payload
    },
    setFilterSortingOption: (state, action) => {
      state.sortingOption = action.payload
    },
    setFilteredGamesLength: (state, action) => {
      state.filteredGamesLength = action.payload
    }
  }
})

export const {
  resetFilters,
  setNameSearch,
  setFilterProviders,
  setFilterGameGroups,
  setFilterSortingOption,
  setFilteredGamesLength
} = filtersSlice.actions
export default filtersSlice.reducer
