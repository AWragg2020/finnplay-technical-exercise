import { useEffect } from 'react'
import { setSessionUser } from './redux/features/sessionUserSlice'
import { useAppDispatch, useAppSelector } from './redux/hooks'

import Catalog from './views/Catalog/Catalog'
import Login from './views/Login'

import './App.css'

function App() {
  const dispatch = useAppDispatch()
  const sessionUser = useAppSelector(state => state.sessionUser.sessionUser)

  useEffect(() => {
    const checkSession = async () => {
      const response = await fetch('http://localhost:5454/session', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      })
      const responseJSON = await response.json()
      dispatch(setSessionUser(responseJSON.sessionUser))
    }
    checkSession()
  }, [])

  return <>{sessionUser ? <Catalog /> : <Login />}</>
}

export default App
