interface TextInputProps {
  label?: string
  placeholder?: string
  innerRef?: any
  onChange?: (e?: any) => void
  iconSrc?: string
  isPassword?: boolean
}

const TextInput = ({
  label,
  placeholder = '',
  innerRef,
  onChange = () => {},
  iconSrc,
  isPassword = false
}: TextInputProps) => {
  return (
    <div className='relative flex w-full items-center'>
      {label && (
        <span className='absolute top-2 left-4 text-sm text-gray-500'>
          {label}
        </span>
      )}
      <input
        onChange={onChange}
        type={isPassword ? 'password' : 'text'}
        ref={innerRef}
        placeholder={placeholder}
        className={`w-full border border-grey rounded-md bg-white px-4 ${
          label ? 'pb-4 pt-8' : 'py-5'
        }`}
      />
      {iconSrc && (
        <img
          src={iconSrc}
          width='16px'
          height='16px'
          className='absolute right-4'
        />
      )}
    </div>
  )
}

export default TextInput
