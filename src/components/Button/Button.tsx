import classes from './Button.module.css'

interface ButtonProps {
  label: string
  onClick: () => void
}

const Button = ({ label, onClick }: ButtonProps) => {
  return (
    <button
      onClick={onClick}
      className={`${classes.button} py-3 text-gray-500`}>
      {label}
    </button>
  )
}

export default Button
