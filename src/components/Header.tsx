import useScreenSize from '../hooks/useScreenSize'
import { resetSessionUser } from '../redux/features/sessionUserSlice'
import { useAppDispatch, useAppSelector } from '../redux/hooks'
import { responsive } from '../utils/breakpoints'

import user from '../assets/icons/user.png'
import logo from '../assets/images/logo.png'

const Header = () => {
  const { width: screenWidth } = useScreenSize()

  const dispatch = useAppDispatch()
  const sessionUser = useAppSelector(state => state.sessionUser.sessionUser)

  const handleLogout = async () => {
    await fetch('http://localhost:5454/logout', {
      method: 'GET'
    })

    dispatch(resetSessionUser())
  }

  return (
    <div
      id='header'
      className={`flex flex-row w-full justify-between items-center ${
        screenWidth <= responsive
          ? 'bg-transparent px-6 pt-3'
          : 'bg-white px-20'
      }`}>
      <img
        src={logo}
        height='70px'
        width='70px'
      />
      <div
        id='user-actions'
        className='flex flex-row gap-10'>
        <span>{sessionUser}</span>
        <div
          id='logout'
          className='select-none cursor-pointer flex flex-row items-center'
          onClick={handleLogout}>
          <img
            src={user}
            className='max-h-4 max-w-4'
          />
          <span className='text-brand ml-2'>Logout</span>
        </div>
      </div>
    </div>
  )
}

export default Header
