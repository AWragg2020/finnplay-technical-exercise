import cors from 'cors'
import express from 'express'
import data from './data/data.json' assert { type: 'json' }

const app = express()
const port = 5454

let sessionUser = null

app.use(cors())
app.use(express.json())

app.get('/session', (req, res) => {
  res.send({ sessionUser: sessionUser })
})

app.post('/login', (req, res) => {
  const user = req.body.user
  const password = req.body.password

  // Timeout to simulate loading
  setTimeout(() => {
    if (
      (user === 'player1' && password === 'player1') ||
      (user === 'player2' && password === 'player2')
    ) {
      sessionUser = user
      res.status(200).send({ sessionUser: sessionUser })
    } else {
      res.status(401).send()
    }
  }, 1500)
})

app.get('/logout', (req, res) => {
  sessionUser = null
  res.send()
})

app.get('/data', (req, res) => {
  res.send(data)
})

app.listen(port, () => {
  console.log(`Backend listenting on port ${port}`)
})
